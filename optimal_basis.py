import tensorflow as tf
import numpy as np
import argparse

import matplotlib
import matplotlib.pyplot as plt
from itertools import combinations
from contextlib import contextmanager

# Limit GPU memory usage to 8Gb
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus : 
    tf.config.experimental.set_virtual_device_configuration(gpus[0], 
         [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024*8)] )

import amplitf.interface as atfi
import amplitf.dynamics as atfd
import amplitf.kinematics as atfk
import amplitf.likelihood as atfl
import tfa.plotting as tfp
import tfa.rootio as tfr
from amplitf.phasespace.dalitz_phasespace import DalitzPhaseSpace

import sys, math, os

# Masses of the initial and final state particles 
md  = 1.869
mpi = 0.139
mk  = 0.497

# Blatt-Weisskopf radial constants
dd = atfi.const(5.)
dr = atfi.const(1.5)

# Auxiliary function to create the resonance of mass "m", width "w", spin "spin" in the 
# channel "ch", as a function of Dalitz plot variables vector "x"
def resonance(x, m, w, spin, ch, dlz) :
    if ch == 0 : 
      return atfd.breit_wigner_lineshape(dlz.m2ab(x), m,  w, mpi, mk, mpi, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2ab(x), dlz.m2ac(x), dlz.m2bc(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mk**2), atfi.const(mpi**2), spin)
    if ch == 1 : 
      return atfd.breit_wigner_lineshape(dlz.m2bc(x), m,  w, mk, mpi, mpi, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2bc(x), dlz.m2ab(x), dlz.m2ac(x), atfi.const(md**2), atfi.const(mk**2), atfi.const(mpi**2), atfi.const(mpi**2), spin)
    if ch == 2 : 
      return atfd.breit_wigner_lineshape(dlz.m2ac(x), m,  w, mpi, mpi, mk, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    if ch == 3 : 
      return atfd.gounaris_sakurai_lineshape(dlz.m2ac(x), m,  w, mpi)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    if ch == 4 : 
      return atfd.flatte_lineshape(dlz.m2ac(x), m, 0.09, 0.02, mpi, mpi, mk, mk)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    return None

class D2KsPiPi : 
  def __init__(self, reduced = False, inputfile = None) : 

    # Create Dalitz plot phase space 
    self.dlz = DalitzPhaseSpace(mpi, mk, mpi, md)

    # Resonance parameters from Belle model fit
    params={"a0r" : ( -0.756841, 0.180804), 
            "a0i" : ( -0.719579, 0.187009),
            "a1r" : (  1.097505, 0.034081),
            "a1i" : (  0.419228, 0.033228),
            "a2r" : ( -0.196752, 0.036479),
            "a2i" : (  0.016840, 0.043689),
            "a3r" : (  0.020875, 0.001096),
            "a3i" : (  0.018290, 0.001195),
            "a4r" : (  0.051761, 0.002613),
            "a4i" : (  0.041137, 0.001897),
            "a5r" : (  0.093466, 0.010187),
            "a5i" : (  0.255268, 0.010483),
            "a6r" : ( -1.357615, 0.069999),
            "a6i" : (  0.040836, 0.059655),
            "a7r" : (  2.988896, 0.127042),
            "a7i" : ( -1.878055, 0.120555),
            "a8r" : (  1.327757, 0.019316),
            "a8i" : ( -1.145395, 0.017091),
            "a9r" : (  0.745753, 0.048381),
            "a9i" : ( -0.744076, 0.052081),
            "a10r" : ( -2.415071, 0.043866),
            "a10i" : ( -1.012651, 0.050998),
            "a11r" : ( -0.686463, 0.030458),
            "a11i" : (  0.131047, 0.030777),
            "a12r" : (  0.042872, 0.300616),
            "a12i" : ( -1.301729, 0.312203),
            "a13r" : ( -0.123027, 0.004991),
            "a13i" : (  0.041238, 0.005045),
            "a14r" : (  0.151902, 0.052192),
            "a14i" : (  0.962249, 0.056412),
            "a15r" : ( -0.528121, 0.037820),
            "a15i" : ( -0.316514, 0.045555),
            "a16r" : ( -0.285350, 0.034573),
            "a16i" : (  0.392055, 0.028871),
            "a17r" : (  0.345416, 0.293911),
            "a17i" : (  3.900965, 0.267996), }

    self.coeffs = []
    for n in range(18) : 
        pname = "a%d" % n
        pr = atfi.const(params[pname + "r"][0])
        pi = atfi.const(params[pname + "i"][0])
        self.coeffs += [ atfi.complex(pr, pi) ]

  def amplitude(self, x) : 
    ampl  = atfi.complex(atfi.const(0.), atfi.const(0.))

    ampl += self.coeffs[0]

    ampl += -atfi.complex(atfi.const(1.), atfi.const(0.))*resonance(x, atfi.const(0.775500), atfi.const(0.149400), 1, 3, self.dlz)
    ampl +=  self.coeffs[1]*resonance(x, atfi.const(0.522477), atfi.const(0.453106), 0, 2, self.dlz)
    ampl += -self.coeffs[2]*resonance(x, atfi.const(1.0),   atfi.const(0.4550),      1, 2, self.dlz)
    ampl += -self.coeffs[3]*resonance(x, atfi.const(0.782650), atfi.const(0.008490), 1, 2, self.dlz)
    ampl +=  self.coeffs[4]*resonance(x, atfi.const(0.97700),  atfi.const(0.1),      0, 4, self.dlz)
    ampl +=  self.coeffs[5]*resonance(x, atfi.const(1.033172), atfi.const(0.087984), 0, 2, self.dlz)
    ampl +=  self.coeffs[6]*resonance(x, atfi.const(1.275400), atfi.const(0.185200), 2, 2, self.dlz)
    ampl +=  self.coeffs[7]*resonance(x, atfi.const(1.434000), atfi.const(0.173000), 0, 2, self.dlz)

    ampl += -self.coeffs[8]*resonance(x, atfi.const(0.891660), atfi.const(0.050800), 1, 0, self.dlz)
    ampl += -self.coeffs[9]*resonance(x, atfi.const(1.414000), atfi.const(0.232000), 1, 0, self.dlz)
    ampl +=  self.coeffs[10]*resonance(x, atfi.const(1.414000), atfi.const(0.290000), 0, 0, self.dlz)
    ampl +=  self.coeffs[11]*resonance(x, atfi.const(1.425600), atfi.const(0.098500), 2, 0, self.dlz)
    ampl += -self.coeffs[12]*resonance(x, atfi.const(1.717000), atfi.const(0.322000), 1, 0, self.dlz)

    ampl += self.coeffs[13]*resonance(x, atfi.const(0.891660), atfi.const(0.050800), 1, 1, self.dlz)
    ampl += self.coeffs[14]*resonance(x, atfi.const(1.414000), atfi.const(0.232000), 1, 1, self.dlz)
    ampl += self.coeffs[15]*resonance(x, atfi.const(1.414000), atfi.const(0.290000), 0, 1, self.dlz)
    ampl += self.coeffs[16]*resonance(x, atfi.const(1.425600), atfi.const(0.098500), 2, 1, self.dlz)
    ampl += self.coeffs[17]*resonance(x, atfi.const(1.717000), atfi.const(0.322000), 1, 1, self.dlz)

    return ampl

  def density(self, x) :
    return atfd.density( self.amplitude(x) )

  def phase(self, x) : 
    a = self.amplitude(x)
    return atfi.atan2(atfi.imaginary(a), atfi.real(a))

def bins(x, c, s, nbins) : 
  """
     Calculate the tensor representing bins over the phase space, following the 
     strong phase difference binning. 
     Inputs: 
       x   : 2D tensor of shape (N, 2) with Dalitz plot variables (N is the number of events)
       c   : vector of cos(strong phase difference)
       s   : vector of sin(strong phase difference)
       nbins : number of bins
     Output: 
       2D tensor of shape (N, 2*nbins) of binary (0/1) values, where 1 means that the input Dalitz plot 
          point belong to a particular bin. 
  """
  phase = atfi.atan2(s, c)
  nbin = (phase/atfi.pi()+1)/2.*nbins
  w = [ atfi.logical_and(x[:,0]<x[:,1], atfi.logical_and(nbin>=i, nbin<i+1)) for i in range(nbins) ] + [
        atfi.logical_and(x[:,0]>x[:,1], atfi.logical_and(nbin>=i, nbin<i+1)) for i in range(nbins) ]
  return atfi.stack(tf.dtypes.cast(w, tf.float64), axis = 1)

@atfi.function
def q2factor(nwghts, c, s) :
  """
    Function to calculate the figure of merit Q^2 of the family of weight functions. 
    Inputs: 
      nwght : list of normalised vectors of weights (normalised such that integral(w**2 * pd)=1, see function below)
      pd    : vector of pD values (pD does not need to be normalised)
      c     : vector of cos(strong phase difference)
      s     : vector of sin(strong phase difference)
    Output: 
      Q^2 value
  """
  q2 = 0.
  for nw in nwghts :
    cw = atfl.integral(c*nw)
    sw = atfl.integral(s*nw)
    q2 += (cw**2 + sw**2)
  return q2

def norm_weights(wght, pd) : 
  """
    Normalise the weight functions
    Inputs: 
      wght : 2D tensor of unnormalised weights
      pd   : vector of pD values (pD does not need to be normalised)
    Output: 
      List of normalised weight functions (such that integral(w**2*pd)=1)
  """
  nwght = []
  for i in range(wght.shape[1]) : 
    nwght += [ wght[:,i] / atfi.sqrt(atfl.integral(pd*wght[:,i]**2)) ]
  return nwght

def covariance(nwght, pd) : 
  """
    Calculate the dictionary representing the covariance matrix
    Inputs: 
      nwght : list of vectors of weights
      pd : vector of pD values
    Output: 
      dictionary of covariances with keys as (n,m)
  """
  corrs = {}
  for i,j in combinations(range(len(nwght)), 2) : 
    if i != j : 
      corr = atfl.integral(pd*nwght[i]*nwght[j])
      corrs[(i,j)] = corr.numpy()
  return corrs

def orthonormalise(v0, v1, v2, v3 = None, W = None) : 
  """
    Calculate the orthonormal basis from the initial 3 or 4 functions 
    using Gram-Schmidt procedure
      v0--v3 are initial "seed" functions
      W is the global weight ("metric" function)
  """
  def proj(v,u) : 
    return u*atfl.integral(u*v*W)/atfl.integral(u**2*W)
  def norm(u) : 
    return u/atfi.sqrt(atfl.integral(u**2*W))
  u0 = v0
  e0 = norm(u0)
  u1 = v1-proj(v1,u0)
  e1 = norm(u1)
  u2 = v2-proj(v2,u0)-proj(v2,u1)
  e2 = norm(u2)
  if not v3 is None : 
    u3 = v3-proj(v3,u0)-proj(v3,u1)-proj(v3,u2)
    e3 = norm(u3)
    return e0,e1,e2,e3
  else : 
    return e0,e1,e2

def main() :

  parser = argparse.ArgumentParser(description = "Optimisation of gamma measurement", 
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--pixels', type=int, default = 500, 
                      help="Number of Dalitz plot pixels")
  parser.add_argument('--output', type=str, default = "output", 
                      help="Output file prefix")

  args = parser.parse_args()

  if len(sys.argv)<=1 : 
    parser.print_help()
    raise SystemExit

  npixels = args.pixels

  dkpp = D2KsPiPi()  # D->Kspipi amplitude object
  dlz = dkpp.dlz     # Dalitz plot phase space object
  bounds = [(dlz.minab, dlz.maxab), (dlz.minbc, dlz.maxbc)]  # Boundaries of Dalitz plot

  x = dlz.rectangular_grid_sample(npixels, npixels).numpy()  # 2D tensor for the grid of Dalitz plot points
  xbar = x[:,(1,0)]                       # The same points reflected wrt. diagonal axis
  ampl = dkpp.amplitude(x).numpy()        # Complex amplitude values in these points
  amplbar = dkpp.amplitude(xbar).numpy()  # Conjugated amplitude values

  pd = atfd.density(ampl)                 # pD values
  pdbar = atfd.density(amplbar)           # bar(pD) values

  pdint = atfl.integral(pd)
  pd /= pdint                # pD and bar(pD) should be normalised to unit integral
  pdbar /= pdint

  ph = atfi.atan2(atfi.imaginary(ampl), atfi.real(ampl))            # Strong phase
  phbar = atfi.atan2(atfi.imaginary(amplbar), atfi.real(amplbar))   # Conjugated strong phase

  dphi = ph - phbar                       # Strong phase difference between symmetric points
  c = atfi.sqrt(pd*pdbar)*atfi.cos(dphi)  # cos(strong phase difference)
  s = atfi.sqrt(pd*pdbar)*atfi.sin(dphi)  # sin(strong phase difference)

  ############################################################################
  ### This part calculates the Q values for some particular weight sets ######
  ############################################################################

  print("Q values for various sets of weight functions: ")

  ########## Strong phase binning with 8 bins 
  nbins = 8
  nwght = norm_weights(bins(x, c, s, nbins), pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q({nbins}bins)={math.sqrt(q2)}")

  ########## Strong phase binning with 20 bins 
  nbins = 20
  nwght = norm_weights(bins(x, c, s, nbins), pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q({nbins}bins)={math.sqrt(q2)}")

  ########## Fourier weights with M=1, not split 
  wght = np.stack([ atfi.ones(pd), atfi.cos(dphi), atfi.sin(dphi) ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q(1,cos,sin)={math.sqrt(q2)}")

  ########## Fourier weights with M=1, split separately for pD<bar(pD) and pd>bar(pD)
  split1 = tf.dtypes.cast(pd>pdbar, tf.float64)
  split2 = tf.dtypes.cast(pd<pdbar, tf.float64)
  wght = np.stack([ 
                    atfi.ones(pd)*split1, atfi.cos(dphi)*split1, atfi.sin(dphi)*split1, 
                    atfi.ones(pd)*split2, atfi.cos(dphi)*split2, atfi.sin(dphi)*split2 
                  ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q(1,cos,sin,split)={math.sqrt(q2)}")

  ########## Fourier weights divided by pD
  wght = np.stack([ atfi.ones(pd)/pd, atfi.cos(dphi)/pd, atfi.sin(dphi)/pd ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q(1/pd,cos/pd,sin/pd)={math.sqrt(q2)}")

  ########## Fourier weights divided by sqrt(pD)
  wght = np.stack([ atfi.ones(pd)/atfi.sqrt(pd), atfi.cos(dphi)/atfi.sqrt(pd), atfi.sin(dphi)/atfi.sqrt(pd) ], axis = 1)
  nwght = norm_weights(wght, pd)
  cov = covariance(nwght, pd)
  q2 = q2factor(nwght, c, s)
  print(f"  Q(1/sqrt(pd),cos/sqrt(pd),sin/sqrt(pd))={math.sqrt(q2)}")
  print(f"     Cov = {cov}")

  ########## Orthonormalised pD, C, S
  basis = orthonormalise(atfi.ones(pd), c/pd, s/pd, W = pd)
  cov = covariance(basis, pd)
  q2 = q2factor(basis, c, s)
  print(f"  Q(1,cos*sqrt(pdbar/pd),sin*sqrt(pdbar/pd))={math.sqrt(q2)}")
  print(f"     Cov = {cov}")

  ########## Orthonormalised pD, pdbar, C, S
  basis = orthonormalise(atfi.ones(pd), pdbar/pd, c/pd, s/pd, W = pd)
  cov = covariance(basis, pd)
  q2 = q2factor(basis, c, s)
  print(f"  Q(1,pdbar/pd,cos*sqrt(pdbar/pd),sin*sqrt(pdbar/pd))={math.sqrt(q2)}")
  print(f"     Cov = {cov}")

  ########## Orthonormalised, symmetrised pD, pdbar, C, S ####
  #### (Note that metric used is W=1) ########################
  sym_basis = orthonormalise(pd+pdbar, c, pd-pdbar, s, W = atfi.ones(pd))
  q2 = q2factor(sym_basis, c, s)
  cov = covariance(sym_basis, atfi.ones(pd))
  print(f"  Q(pd,pdbar,C,S,ortho,symmetrised)={math.sqrt(q2)}")
  print(f"     Cov = {cov}")

  M = len(basis)

  print("Other auxiliary information: ")

  cint = atfl.integral(c)/atfl.integral(pd)
  sint = atfl.integral(s)/atfl.integral(pd)
  print(f"  integral(C)={cint}, integral(S)={sint}")

  cosint = atfl.integral(atfi.cos(dphi))
  sinint = atfl.integral(atfi.sin(dphi))
  print(f"  integral(cos)={cosint}, integral(sin)={sinint}")

  # Open graphic window
  tfp.set_lhcb_style(size = 12, usetex = True)
  labels = (r"$M^2(K_S^0\pi^+)$", r"$M^2(K_S^0\pi^-)$")

  @contextmanager
  def plot(name, prefix) : 
    fig, ax = plt.subplots(figsize = (3.5, 3) )
    fig.subplots_adjust(bottom=0.18, left = 0.18, right = 0.95, top = 0.9)
    yield fig, ax
    fig.savefig(prefix + name + ".pdf")
    fig.savefig(prefix + name + ".png")

  with plot("_dlz", args.output) as (fig, ax) : 
    tfp.plot_distr2d(x[:,0], x[:,1], (npixels-1, npixels-1), bounds + [(np.amax(pd)/1e3, np.amax(pd))], 
                   fig, ax, labels = labels, weights = pd, log = True,
                   ztitle = r"$p_{D}(z)$", title = r"$p_{D}(z)$")

  for n in range(M) : 
    with plot(f"_e{n}", args.output) as (fig, ax) : 
      tfp.plot_distr2d(x[:,0], x[:,1], (npixels-1, npixels-1), bounds + [(-4.0, 4.0)], fig, ax, labels = labels, 
                       weights = (basis[n]), colorbar = True, cmap = "seismic", title = f"$e_{{{n}}}(z)$", ztitle = f"$e_{{{n}}}(z)$")

  plt.show()

  np.savez(f"{args.output}.npz", x=x, nwght=nwght)
  tfr.write_tuple(f"{args.output}.root", np.stack([x[:,0], x[:,1], pd, pdbar, c, s] + list(basis), axis=1), 
                      branches = ["x","y","pd","pdb","c","s"] + [f"w{i}" for i in range(M)])

if __name__ == "__main__" : 
  main()
