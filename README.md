# Optimisation of weighted model-independent CKM gamma measurement 

## Introduction

This project is a further extension of the method to perform the model-independent measurement of the CKM angle $`\gamma`$ in a statistically more optimal way than the binned procedure [1]. The alternative technique that was proposed is based on using the family of weight functions rather than bins. It was shown that using Fourier transformation of strong phase difference, better statistical accuracy can be achieved, although it is still somewhat worse than the unbinned approach [2,3]. 

In this project, the attempt is made to obtain the set of weight functions that would provide the best possible statistical sensitivity. Details on the construction of the figure of merit and the set of weight functions are given in [4]. 

## Prerequisites

To run the code, you need two TensorFlow-based packages, [AmpliTF](https://github.com/apoluekt/AmpliTF) and [TensorFlowAnalysis2](https://github.com/apoluekt/TFA2). Instructions how to install these packages, together with the TensorFlow environment using Conda, can be found [here](https://github.com/apoluekt/TFA2/blob/master/doc/01_installation.md). 

## Running the code

Once the installation is done, activate the Conda `tfa` environment and run the [runme.sh](https://gitlab.cern.ch/poluekt/weightedgammaoptimisation/-/blob/master/runme.sh) shell script provided: 
```bash
$ conda activate tfa
$ . runme.sh
```
The script will calculate Q values for various sets of weights (binned with 8 and 20 bins, Fourier non-split, Fourier split, optimal with 3 and 4 basis functions, and symmetrised basis). Note that the Q value for symmetrised basis is not equal to 1 because Q is calculated in the assumption of no correlations between pB weighted coefficients, while in the symmetrised basis these correlations are not zero. 

Finally, for the set of 4 basis functions, the plots, numpy .npz and ROOT files are produced. 

## Documentation

   1. Binned model-independent approach: https://arxiv.org/abs/0801.0840
   1. Unbinned Fourier-based approach: https://arxiv.org/abs/1712.08326
   1. Fourier-based code repository: https://gitlab.cern.ch/poluekt/FourierGamma
   1. Slides from the "Fourier GGSZ group meeting", [5 Feb 2021](https://indico.cern.ch/event/999977/#2-optimisation-of-weighting-fu)
